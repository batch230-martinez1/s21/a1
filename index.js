let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
function addToOGArr(input) {
        users[users.length] = input;
      }
  
 addToOGArr("John Cena");
  console.log(users);




//  2
let itemFound;

function getItemByIndex(index) {
  return users[index];
}

itemFound = getItemByIndex(2);
console.log(itemFound);


// 3
console.log(users[4])


function deleteLastItem() {
    let lastItem = users[users.length - 1];
    users.length = users.length - 1;
    return lastItem;
  }
  
  let deletedItem = deleteLastItem();
  console.log(users);


// 4
function updateArray(update, index) {
    users[index] = update;
  }
  
  updateArray("Triple H", users.length-1);
  console.log(users);




// 5
function emptyArray() {
    users.length = 0;
  }
  
  emptyArray();
  console.log(users);



//  6
  let isUsersEmpty;

  function checkIfArrayEmpty() {
    if (users.length > 0) {
      return false;
    } else {
      return true;
    }
  }
  
  isUsersEmpty = checkIfArrayEmpty();
  console.log(isUsersEmpty);



